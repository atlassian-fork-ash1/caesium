package com.atlassian.scheduler.caesium.migration;

/**
 * A stub of one of Quartz's classes, for migration purposes.
 *
 * @since v0.0.3
 */
@SuppressWarnings("ClassTooDeepInInheritanceTree")  // Tell that to Quartz :P
public class JobDataMap extends StringKeyDirtyFlagMap {
    private static final long serialVersionUID = -6939901990106713909L;

    protected Object readResolve() {
        return unwrap().get("parameters");
    }
}
