package com.atlassian.scheduler.caesium.cron.rule.field;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

/**
 * Rule based on target day of month that uses either of the {@code L} or {@code W} flags and is therefore
 * guaranteed to match at most a single day in any given month.
 *
 * @since v0.0.1
 */
public class SpecialDayOfMonthFieldRule extends SpecialDayFieldRule {
    private static final long serialVersionUID = -8821720295413352749L;

    private final int target;
    private final boolean nearestWeekday;

    public SpecialDayOfMonthFieldRule(int target, boolean nearestWeekday) {
        super();
        this.target = target;
        this.nearestWeekday = nearestWeekday;
    }

    @Override
    int calculateMatchingDay(final int year, final int month) {
        final LocalDate firstOfMonth = new LocalDate(year, month, 1);
        final int lastDay = firstOfMonth.dayOfMonth().getMaximumValue();
        final int target = this.target;

        // If target is non-positive, then we have an L-x or L-xW rule where target is -x
        if (target <= 0) {
            return calculateRelativeToLastDay(firstOfMonth, lastDay, target);
        }

        // A nice, simple target within the month's bounds only needs to be filtered with the nearest weekday rule
        if (target <= lastDay) {
            return applyNearestWeekdayRule(firstOfMonth, target, target);
        }

        // If the target date exceeds the number of days this month, then we could argue that the nearest weekday rule
        // should still be able to save it.  But it doesn't for Quartz (except in one specific edge case that is
        // clearly a bug), so at least for now it won't for us, either.
        return -1;
    }

    /**
     * Applies the {@code L-x} and {@code L-xW} rule formats.
     *
     * @param firstOfMonth a representation of the first day of the month this year
     * @param lastDay      the last day of month value; equal to {@code firstOfMonth.dayOfMonth().getMaximumValue()}
     * @param offset       the value {@code -x} from the rule.  For example, when the rule is {@code L-3W}, the value
     *                     of {@code offset} is {@code -3}.  For {@code L} or {@code LW}, it is {@code 0}.
     * @return the day of the month that matches this rule, or {@code -1} if it cannot be matched by any day this month
     */
    private int calculateRelativeToLastDay(final LocalDate firstOfMonth, final int lastDay, final int offset) {
        final int day = lastDay + offset;

        // If this lands us on a day within this month, then use it as if it had been specified directly
        if (day > 0) {
            return applyNearestWeekdayRule(firstOfMonth, day, day);
        }

        // L-x or L-xW where x was large enough to move us past the 1st of the month.
        // If the nearest weekday flag is active ('W' was specified), then it takes effect from the
        // 1st of the month; otherwise, there is simply no match for this month.
        if (nearestWeekday) {
            return calculateNearestWeekday(firstOfMonth);
        }
        return -1;
    }

    /**
     * Find the closest weekday to an ideal day of the month or return an alternative value when the
     * {@code W} flag has not been specified.
     *
     * @param firstOfMonth a representation of the first day of the month this year
     * @param idealDay     the ideal day of the month to aim for
     * @param orElse       the value to return when the {@code W} flag has not been specified, meaning only
     *                     an exact match will be accepted
     * @return {@code orElse} if the nearest weekday rule is not relevant; {@code idealDay} if it represents
     * a weekday as-is; otherwise, the day of the month closest to {@code idealDay} that is
     * both still within this month and a weekday.
     */
    private int applyNearestWeekdayRule(final LocalDate firstOfMonth, final int idealDay, final int orElse) {
        if (nearestWeekday) {
            return calculateNearestWeekday(firstOfMonth.withDayOfMonth(idealDay));
        }
        return orElse;
    }

    /**
     * Finds the closest weekday to the provided date.
     * <p>
     * The value is chosen as follows, where {@code day} is taken to be the day of month for the supplied
     * ideal date and {@code lastDay} is the last day of the month:
     * </p>
     * <table>
     * <thead>
     * <tr><th>Condition</th><th>Value chosen</th></tr>
     * </thead>
     * <tbody>
     * <tr><td>Saturday ({@code day == 1})</td><td>{@code 3} (the following Monday)</td></tr>
     * <tr><td>Saturday ({@code day != 1})</td><td>{@code day - 1} (the preceding Friday)</td></tr>
     * <tr><td>Sunday ({@code day == lastDay})</td><td>{@code day - 2} (the preceding Friday)</td></tr>
     * <tr><td>Sunday ({@code day != lastDay})</td><td>{@code day + 1} (the following Monday)</td></tr>
     * </tbody>
     * </table>
     *
     * @param idealDate the desired date
     * @return the day of the month
     */
    private static int calculateNearestWeekday(final LocalDate idealDate) {
        switch (idealDate.getDayOfWeek()) {
            case DateTimeConstants.SATURDAY:
                return calculateNearestWeekdayFromSaturday(idealDate);
            case DateTimeConstants.SUNDAY:
                return calculateNearestWeekdayFromSunday(idealDate);
        }
        return idealDate.dayOfMonth().get();
    }

    private static int calculateNearestWeekdayFromSaturday(final LocalDate idealDate) {
        final int day = idealDate.getDayOfMonth();

        // For Saturday the 1st, return Monday the 3rd
        if (day == 1) {
            return 3;
        }

        // For anything else, the previous day is the desired Friday
        return day - 1;
    }

    private static int calculateNearestWeekdayFromSunday(final LocalDate idealDate) {
        final int day = idealDate.getDayOfMonth();

        // For Sunday as the last day of the month, return the preceding Friday
        if (day == idealDate.dayOfMonth().getMaximumValue()) {
            return day - 2;
        }

        // For anything else, the following day is the desired Monday
        return day + 1;
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        if (target > 0) {
            sb.append(target);
        } else {
            sb.append('L');
            if (target < 0) {
                sb.append(target);
            }
        }
        if (nearestWeekday) {
            sb.append('W');
        }
    }
}

