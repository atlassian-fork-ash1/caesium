package com.atlassian.scheduler.caesium.migration;

import com.atlassian.scheduler.core.util.ParameterMapSerializer;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Map;

/**
 * An implementation of the {@code ParameterMapSerializer} that uses {@link LazyMigratingObjectInputStream}
 * to discard the Quartz-specific {@code JobDataMap} classes that possibly wrap them.
 * <p>
 * This is meant to make it easier to migrate from Quartz to Caesium.
 * </p>
 *
 * @since v0.0.3
 */
public class LazyMigratingParameterMapSerializer extends ParameterMapSerializer {
    protected ObjectInputStream createObjectInputStream(ClassLoader classLoader, byte[] parameters) throws IOException {
        return new LazyMigratingObjectInputStream(classLoader, parameters);
    }

    @Nonnull
    @Override
    public Map<String, Serializable> deserializeParameters(ClassLoader classLoader, @Nullable byte[] parameters)
            throws ClassNotFoundException, IOException {
        if (parameters != null) {
            Object result = deserializeBytes(classLoader, parameters);

            // We should have either a Map or null.  We can only get a byte[] if the outermost class was
            // the Quartz JobDataMap and we stripped it, but that means we need to make another deserialization
            // pass with the real serialized parameters.
            if (result instanceof byte[]) {
                result = deserializeBytes(classLoader, (byte[]) result);
            }

            if (result != null) {
                //noinspection unchecked
                return (Map<String, Serializable>) result;
            }
        }

        return ImmutableMap.of();
    }

    @Nullable
    private Object deserializeBytes(ClassLoader classLoader, @Nullable byte[] parameters)
            throws ClassNotFoundException, IOException {
        if (parameters == null) {
            return ImmutableMap.of();
        }

        final ObjectInputStream in = createObjectInputStream(classLoader, parameters);
        try {
            return in.readObject();
        } finally {
            in.close();
        }
    }
}
