package com.atlassian.scheduler.caesium.cron.rule;

import com.atlassian.scheduler.caesium.cron.rule.field.FieldRule;

import static com.atlassian.scheduler.caesium.cron.rule.CompositeRule.compose;
import static java.util.Objects.requireNonNull;

/**
 * Composes a set of parsed cron fields into a container that can evaluate all of them
 * and find the next matching time for the expression.
 *
 * @since v0.0.1
 */
public class CronExpression implements CronRule {
    private static final long serialVersionUID = -5039113142559402448L;

    private final String cronExpression;
    private final CronRule delegate;

    public CronExpression(
            final String cronExpression,
            final FieldRule year, final FieldRule month, final FieldRule day,
            final FieldRule hour, final FieldRule minute, final FieldRule second) {
        this.cronExpression = requireNonNull(cronExpression, "cronExpression");

        CronRule rule = second;
        rule = compose(minute, rule);
        rule = compose(hour, rule);
        rule = compose(day, rule);
        rule = compose(month, rule);
        this.delegate = compose(year, rule);
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        return delegate.matches(dateTime);
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        return delegate.next(dateTime);
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        return delegate.first(dateTime);
    }

    @Override
    public String toString() {
        return "CronExpression[" + cronExpression + ":\n\t" + delegate + "\n]";
    }
}
