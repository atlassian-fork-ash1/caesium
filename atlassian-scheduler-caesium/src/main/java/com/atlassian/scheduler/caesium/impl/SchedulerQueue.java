package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;

import javax.annotation.Nullable;
import java.io.Closeable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * A blocking, closeable priority queue for implementing a simple scheduler.
 *
 * @since v0.0.1
 */
interface SchedulerQueue extends Closeable {
    /**
     * Enqueues the specified pending job request.
     * <p>
     * This implicitly cancels any previously scheduled request with the same {@link QueuedJob#getJobId() jobId}.
     * </p>
     *
     * @param job the pending job request to add to this queue
     * @throws SchedulerShutdownException if the queue is already {@link #close() closed}
     */
    void add(final QueuedJob job) throws SchedulerShutdownException;

    /**
     * Refreshes the clustered jobs in the queue.
     * <p>
     * This is equivalent to calling {@link #add(QueuedJob)} for each job currently registered with the
     * {@link com.atlassian.scheduler.caesium.spi.ClusteredJobDao}, except that the entire refresh is
     * done under a single lock and should be significantly more efficient.
     * </p>
     *
     * @return a mapping of all scheduled job IDs to their next run times
     */
    Map<JobId, Date> refreshClusteredJobs();

    /**
     * Removes the pending job request with the specified {@code jobId}.
     *
     * @param jobId the pending job request to remove from the queue
     * @return the pending job that was removed from the queue
     */
    @Nullable
    QueuedJob remove(final JobId jobId);

    /**
     * Wait for a pending job to become due and remove it from the queue.
     * <ul>
     * <li>If the queue is closed, then {@code null} is returned.</li>
     * <li>If the next pending job is due and the queue is not {@link #pause() paused}, then that
     * job is removed from the queue and returned.</li>
     * <li>Otherwise, {@code take()} blocks until one of these conditions is satisfied or the
     * current thread is interrupted.</li>
     * </ul>
     *
     * @return the next job to run, or {@code null} if the queue has been {@link #close() closed}
     * @throws InterruptedException if the current thread is interrupted while waiting for a job
     */
    @Nullable
    QueuedJob take() throws InterruptedException;

    /**
     * Temporarily suspends the taking of jobs from the queue.
     * <p>
     * Any thread that has called {@link #take()} will remain blocked in that call until the queue
     * is {@link #close() closed} or {@link #resume() resumed} or that thread is interrupted.
     * Calling {@code pause()} more than once has no special side-effects.
     * </p>
     *
     * @throws SchedulerShutdownException if the queue is already {@link #close() closed}
     * @see #resume()
     */
    void pause() throws SchedulerShutdownException;

    /**
     * Re-enables taking of jobs from the queue.
     * <p>
     * This removes the block raised by an earlier call to {@link #pause()}, regardless of how many
     * times {@code pause()} might have been called.  Calling {@code resume()} more than once has
     * no special side-effects.
     * </p>
     *
     * @throws SchedulerShutdownException if the queue is already {@link #close() closed}
     * @see #pause()
     */
    void resume() throws SchedulerShutdownException;

    /**
     * Returns {@code true} if the queue has been {@link #close() closed}.
     *
     * @return {@code true} if the queue has been {@link #close() closed}; {@code false} otherwise.
     */
    boolean isClosed();

    /**
     * Permanently closes the scheduler queue.
     * <p>
     * Once the queue is closed:
     * </p>
     * <ul>
     * <li>All pending or subsequent calls to {@link #take()} return {@code null}.</li>
     * <li>All subsequent calls to {@link #refreshClusteredJobs()} are ignored and return an empty map.</li>
     * <li>All subsequent calls to {@link #add(QueuedJob)}, {@link #pause()}, or {@link #resume()},
     * throw {@link SchedulerShutdownException}.</li>
     * <li>The job queue is cleared; subsequent calls to {@link #getPendingJobs()} return an
     * empty list.</li>
     * </ul>
     */
    @Override
    void close();

    /**
     * Returns a list of the jobs that are currently scheduled to run, ordered by their scheduled run time.
     *
     * @return a list of the jobs that are currently scheduled to run, ordered by their scheduled run time.
     */
    List<QueuedJob> getPendingJobs();


    class SchedulerShutdownException extends SchedulerServiceException {
        private static final long serialVersionUID = 38756229754957063L;

        public SchedulerShutdownException() {
            super("The scheduler has been shutdown.");
        }
    }
}
