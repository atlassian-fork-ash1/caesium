package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * A memory-based partial implementation of {@code ClusteredJobDao} that is not fully compliant with the SPI.
 * <p>
 * <strong>WARNING</strong>: Although otherwise perfectly functional, this implementation is purely RAM-based,
 * so it does not persist clustered jobs across restarts.  This violates the general contract of the
 * atlassian-scheduler API.  In particular, see {@link RunMode#RUN_ONCE_PER_CLUSTER} and {@link ClusteredJobDao},
 * which both make it clear that clustered jobs should survive an application restart.  It would therefore be
 * inappropriate to use this implementation except in very special cases, like functional testing or "dev mode".
 * Even applications that do not support clustering should avoid using this implementation, as failing to
 * preserve the jobs across restarts might confuse cross-product plugins.
 * </p>
 */
public class MemoryClusteredJobDao implements ClusteredJobDao {
    private static final int INITIAL_STORE_SIZE = 256;

    private final ConcurrentMap<JobId, ClusteredJob> store = new ConcurrentHashMap<JobId, ClusteredJob>(INITIAL_STORE_SIZE);

    @Nullable
    @Override
    public Date getNextRunTime(JobId jobId) {
        final ClusteredJob job = store.get(jobId);
        return (job != null) ? job.getNextRunTime() : null;
    }

    @Nullable
    @Override
    public Long getVersion(JobId jobId) {
        final ClusteredJob job = store.get(jobId);
        return (job != null) ? job.getVersion() : null;
    }

    @Nullable
    @Override
    public ClusteredJob find(JobId jobId) {
        return store.get(jobId);
    }

    @Nonnull
    @Override
    public Collection<ClusteredJob> findByJobRunnerKey(final JobRunnerKey jobRunnerKey) {
        final ImmutableList.Builder<ClusteredJob> jobs = ImmutableList.builder();
        for (ClusteredJob job : store.values()) {
            if (job.getJobRunnerKey().equals(jobRunnerKey)) {
                jobs.add(job);
            }
        }
        return jobs.build();
    }

    @Nonnull
    @Override
    public Map<JobId, Date> refresh() {
        final ImmutableMap.Builder<JobId, Date> jobs = ImmutableMap.builder();
        for (ClusteredJob job : store.values()) {
            final Date nextRunTime = job.getNextRunTime();
            if (nextRunTime != null) {
                jobs.put(job.getJobId(), nextRunTime);
            }
        }
        return jobs.build();
    }

    @Nonnull
    @Override
    public Set<JobRunnerKey> findAllJobRunnerKeys() {
        final ImmutableSet.Builder<JobRunnerKey> keys = ImmutableSet.builder();
        for (ClusteredJob job : store.values()) {
            keys.add(job.getJobRunnerKey());
        }
        return keys.build();
    }

    @Override
    public boolean create(ClusteredJob clusteredJob) {
        return store.putIfAbsent(clusteredJob.getJobId(), clusteredJob) == null;
    }

    @Override
    public boolean updateNextRunTime(JobId jobId, @Nullable Date nextRunTime, long expectedVersion) {
        final ClusteredJob existing = store.get(jobId);
        if (existing == null) {
            return false;
        }

        final ClusteredJob updated = ImmutableClusteredJob.builder(existing)
                .version(existing.getVersion() + 1L)
                .nextRunTime(nextRunTime)
                .build();
        return store.replace(jobId, existing, updated);
    }

    @Override
    public boolean delete(JobId jobId) {
        return store.remove(jobId) != null;
    }
}