package com.atlassian.scheduler.caesium.migration;

import com.atlassian.scheduler.core.util.ClassLoaderAwareObjectInputStream;
import com.google.common.collect.ImmutableMap;

import java.io.IOException;
import java.io.ObjectStreamClass;
import java.util.Map;

/**
 * An object input stream that removes the Quartz JobDataMap class from the stream, returning only the
 * {@code "parameters"} value.  This should be a {@code byte[]} of the actual serialized job parameters
 * if the Quartz job was created by the Atlassian Scheduler library.
 *
 * @since v0.0.3
 */
public class LazyMigratingObjectInputStream extends ClassLoaderAwareObjectInputStream {
    private static final Map<String, Class<?>> CLASS_MAPPER = ImmutableMap.<String, Class<?>>builder()
            .put("org.quartz.JobDataMap", JobDataMap.class)
            .put("org.quartz.utils.StringKeyDirtyFlagMap", StringKeyDirtyFlagMap.class)
            .put("org.quartz.utils.DirtyFlagMap", DirtyFlagMap.class)
            .build();

    public LazyMigratingObjectInputStream(ClassLoader classLoader, byte[] parameters) throws IOException {
        super(classLoader, parameters);
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        final Class<?> killed = CLASS_MAPPER.get(desc.getName());
        return (killed != null) ? killed : super.resolveClass(desc);
    }
}
