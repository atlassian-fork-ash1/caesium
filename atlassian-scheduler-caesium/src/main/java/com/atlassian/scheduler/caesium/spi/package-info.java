/**
 * The external dependencies that an application must provide to make use of atlassian-scheduler-caesium.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.caesium.spi;

import javax.annotation.ParametersAreNonnullByDefault;