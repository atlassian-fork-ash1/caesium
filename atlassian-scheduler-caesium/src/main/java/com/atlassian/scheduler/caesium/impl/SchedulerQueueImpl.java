package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.config.JobId;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.annotation.concurrent.GuardedBy;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.Collections.sort;
import static java.util.Objects.requireNonNull;

/**
 * A simple scheduling queue.
 * <p>
 * The queue is maintained internally in two forms:
 * </p>
 * <ol>
 * <li><code>PriorityQueue</code> &mdash; This has good timing characteristics for identifying which job will
 * be the next one that runs without us having to maintain a fully sorted list of them.</li>
 * <li><code>HashMap</code> &mdash; Ensures uniqueness and fast removal by {@code JobId}</li>
 * </ol>
 *
 * @since v0.0.1
 */
class SchedulerQueueImpl implements SchedulerQueue {
    private static final Logger LOG = LoggerFactory.getLogger(SchedulerQueueImpl.class);

    // Could make this tunable, but it seems unlikely to have much impact...
    private static final int QUEUE_SIZE_HINT = 256;

    @GuardedBy("lock")
    private final PriorityQueue<QueuedJob> queue = new PriorityQueue<>(QUEUE_SIZE_HINT);

    @GuardedBy("lock")
    private final Map<JobId, QueuedJob> jobsById = new HashMap<>(QUEUE_SIZE_HINT);

    private final ClusteredJobDao clusteredJobDao;
    private final Lock lock;
    private final Condition awaken;

    private volatile boolean closed;
    private volatile boolean paused;

    SchedulerQueueImpl(ClusteredJobDao clusteredJobDao) {
        this(clusteredJobDao, new ReentrantLock());
    }

    @VisibleForTesting
    SchedulerQueueImpl(ClusteredJobDao clusteredJobDao, Lock lock) {
        this.clusteredJobDao = requireNonNull(clusteredJobDao, "clusteredJobDao");
        this.lock = requireNonNull(lock, "lock");
        this.awaken = lock.newCondition();
    }

    @Override
    public void add(final QueuedJob job) throws SchedulerShutdownException {
        requireNonNull(job, "job");

        final QueuedJob replaced;
        lock.lock();
        try {
            replaced = addJobUnderLock(job);
        } finally {
            lock.unlock();
        }

        LOG.debug("add job={} replaced={}", job, replaced);
    }

    @GuardedBy("lock")
    private QueuedJob addJobUnderLock(final QueuedJob job) throws SchedulerShutdownException {
        ensureOpen();

        final QueuedJob replaced = addOrReplaceJob(job);
        //noinspection ObjectEquality
        if (!paused && queue.peek() == job) {
            awaken.signalAll();
        }
        return replaced;
    }

    @GuardedBy("lock")
    private QueuedJob addOrReplaceJob(final JobId jobId, long deadline) {
        return addOrReplaceJob(new QueuedJob(jobId, deadline));
    }

    @GuardedBy("lock")
    private QueuedJob addOrReplaceJob(final QueuedJob job) {
        final QueuedJob replaced = jobsById.put(job.getJobId(), job);
        if (replaced != null) {
            queue.remove(replaced);
        }
        queue.add(job);
        return replaced;
    }


    @Override
    public Map<JobId, Date> refreshClusteredJobs() {
        lock.lock();
        try {
            return refreshClusteredJobsUnderLock();
        } finally {
            lock.unlock();
        }
    }

    @GuardedBy("lock")
    private Map<JobId, Date> refreshClusteredJobsUnderLock() {
        if (closed) {
            return ImmutableMap.of();
        }

        final long originalDeadline = nextJobDeadline();
        final Map<JobId, Date> jobs = refreshClusteredJobsFromDao();
        if (!paused && nextJobDeadline() < originalDeadline) {
            awaken.signalAll();
        }
        return jobs;
    }

    @GuardedBy("lock")
    private Map<JobId, Date> refreshClusteredJobsFromDao() {
        final Map<JobId, Date> jobs = clusteredJobDao.refresh();
        for (Map.Entry<JobId, Date> entry : jobs.entrySet()) {
            final JobId jobId = entry.getKey();
            final Date nextRunTime = entry.getValue();
            if (jobId != null && nextRunTime != null) {
                addOrReplaceJob(jobId, nextRunTime.getTime());
            }
        }
        return jobs;
    }

    @GuardedBy("lock")
    private long nextJobDeadline() {
        final QueuedJob nextJob = queue.peek();
        return (nextJob != null) ? nextJob.getDeadline() : Long.MAX_VALUE;
    }


    @Nullable
    @Override
    public QueuedJob remove(final JobId jobId) {
        requireNonNull(jobId, "jobId");

        final QueuedJob removed;
        lock.lock();
        try {
            removed = removeUnderLock(jobId);
        } finally {
            lock.unlock();
        }

        LOG.debug("remove jobId={} removed={}", jobId, removed);
        return removed;
    }

    @GuardedBy("lock")
    private QueuedJob removeUnderLock(final JobId jobId) {
        final QueuedJob removed = jobsById.remove(jobId);
        if (removed != null) {
            queue.remove(removed);
        }
        return removed;
    }


    @Override
    public boolean isClosed() {
        return closed;
    }


    @Override
    public void close() {
        lock.lock();
        try {
            closeUnderLock();
        } finally {
            lock.unlock();
        }
    }

    @GuardedBy("lock")
    private void closeUnderLock() {
        if (closed) {
            return;
        }

        closed = true;
        jobsById.clear();
        queue.clear();
        awaken.signalAll();
    }


    @Override
    public void pause() throws SchedulerShutdownException {
        lock.lock();
        try {
            ensureOpen();
            paused = true;
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void resume() throws SchedulerShutdownException {
        lock.lock();
        try {
            ensureOpen();
            if (paused) {
                paused = false;
                awaken.signalAll();
            }
        } finally {
            lock.unlock();
        }
    }


    @Nullable
    @Override
    public QueuedJob take() throws InterruptedException {
        while (!closed) {
            final QueuedJob nextJob;
            lock.lock();
            try {
                nextJob = takeUnderLock();
            } finally {
                lock.unlock();
            }

            if (nextJob != null) {
                LOG.debug("take: {}", nextJob);
                return nextJob;
            }

            LOG.debug("take: null (loop)");
        }

        LOG.debug("take: null (closed)");
        return null;
    }

    /**
     * Polls the queue for a job that is ready to take.
     * <p>
     * If no such job is immediately available, then this will block until the thread is awakened.
     * There are a number of events that can awaken the thread:
     * </p>
     * <ol>
     * <li>The minimum sleep time has elapsed, so the next pending job at the time this method was called is expected
     * to now be ready.</li>
     * <li>A call to {@link #add(QueuedJob)} invalidates the previous minimum sleep time.</li>
     * <li>The queue is {@link #resume() resumed} after previously being {@link #pause() paused}.</li>
     * <li>The queue is permanently {@link #close() closed}</li>
     * <li>A <em>spurious wakeup</em> occurs, as is documented for {@link #wait(long)}.</li>
     * </ol>
     * <p>
     * In all such cases, {@code null} is returned so that {@link #take()} can check whether or not the
     * queue has been closed before the queue is polled again.
     * </p>
     *
     * @return a job that is immediately due to run if there is one; {@code null} if the method had blocked
     * and has just been awakened for one of the reasons given above
     * @throws InterruptedException if the thread was interrupted while waiting
     */
    @SuppressWarnings("AwaitNotInLoop")  // The while (!closed) loop in take() covers this
    @GuardedBy("lock")
    @Nullable
    private QueuedJob takeUnderLock() throws InterruptedException {
        final QueuedJob nextJob = queue.peek();
        if (nextJob == null || paused) {
            awaken.await();
            return null;
        }

        // Note: sign overflows can't happen because both of these values are guaranteed to be non-negative.
        final long timeleft = nextJob.getDeadline() - now();
        if (timeleft <= 0L) {
            jobsById.remove(nextJob.getJobId());
            return queue.poll();
        }

        awaken.await(timeleft, TimeUnit.MILLISECONDS);
        return null;
    }

    @Override
    public List<QueuedJob> getPendingJobs() {
        final List<QueuedJob> list = new ArrayList<>(QUEUE_SIZE_HINT);
        lock.lock();
        try {
            list.addAll(queue);
        } finally {
            lock.unlock();
        }
        sort(list);
        return ImmutableList.copyOf(list);
    }

    @VisibleForTesting
    long now() {
        return System.currentTimeMillis();
    }

    private void ensureOpen() throws SchedulerShutdownException {
        if (closed) {
            throw new SchedulerShutdownException();
        }
    }
}
