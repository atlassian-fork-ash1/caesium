package com.atlassian.scheduler.caesium.cron.rule;

import static java.util.Objects.requireNonNull;

/**
 * Composes a "major" and "minor" rule into a single rule.
 * <p>
 * This is not a symmetric composition.  The "major" rule is expected to cover larger intervals
 * and will be moved first when it does not match.  The "minor" rule is adjusted only when then
 * major rule matches and is always reset when the "major" rule is moved to a new value.
 * </p>
 *
 * @since v0.0.1
 */
class CompositeRule implements CronRule {
    private static final long serialVersionUID = -8319039114000087612L;

    private final CronRule major;
    private final CronRule minor;

    static CompositeRule compose(final CronRule major, final CronRule minor) {
        return new CompositeRule(major, minor);
    }

    private CompositeRule(final CronRule major, final CronRule minor) {
        this.major = requireNonNull(major, "major");
        this.minor = requireNonNull(minor, "minor");
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        return major.matches(dateTime) && minor.matches(dateTime);
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        // If "major" already matches and there are still values left for "minor" then we are done
        if (major.matches(dateTime) && minor.next(dateTime)) {
            return true;
        }

        // Otherwise we need to scan the rest of the "major" values that match until we find a "minor" to go with it
        while (major.next(dateTime)) {
            if (minor.first(dateTime)) {
                return true;
            }
        }

        // All "major" values are exhausted
        return false;
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        return major.first(dateTime) && minor.first(dateTime);
    }

    @Override
    public String toString() {
        return major + "\n\t" + minor;
    }
}
