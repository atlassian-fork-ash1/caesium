package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.CronRule;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;

/**
 * A container for a field value that can modify its value according to whatever constraints were defined in
 * a cron expression.
 *
 * @since v0.0.1
 */
public interface FieldRule extends CronRule {
    /**
     * Returns the currently set value for this field.
     *
     * @param dateTime the moment from which to extract this field's value
     * @return the value for this field as currently set in {@code dateTime}
     */
    int get(DateTimeTemplate dateTime);

    /**
     * Modifies the value for this field to produce a new moment for consideration.
     *
     * @param dateTime the moment to modify
     * @param value    the value to set for this field
     */
    void set(DateTimeTemplate dateTime, int value);
}

