package com.atlassian.scheduler.caesium.spi;

import com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;

/**
 * Provides custom configuration settings for the Caesium scheduler service.
 *
 * @since v0.0.1
 */
public interface CaesiumSchedulerConfiguration extends SchedulerServiceConfiguration {
    /**
     * Returns the frequency with which the scheduler will perform routine cluster maintenance.
     * <p>
     * If the value is positive, then the scheduler will implicitly register a local maintenance
     * task that calls {@link CaesiumSchedulerService#refreshClusteredJobs()} at the specified
     * interval.
     * </p><p>
     * If the value is not positive, then the application must make its own arrangements to notify other
     * cluster nodes about new clustered jobs.  For example, it might use a message passing service to
     * explicitly inform other nodes that a new cluster-wide job has been scheduled.  The nodes that receive
     * such a message could then call {@link CaesiumSchedulerService#refreshClusteredJob(JobId)} to ensure
     * that the new job is included in the scheduler's work queue.
     * </p><p>
     * It would also be appropriate to disable this for any single-node application, as the database
     * poll to check for updated clustered jobs would just be wasteful.
     * </p>
     *
     * @return the frequency (in minutes) at which the scheduler will automatically poll for changes to
     * clustered jobs, or a non-positive value to disable this feature
     * @see CaesiumSchedulerService#refreshClusteredJobs()
     * @see CaesiumSchedulerService#refreshClusteredJob(JobId)
     */
    int refreshClusteredJobsIntervalInMinutes();

    /**
     * Returns the number of worker threads that will accept jobs from the queue.
     * <p>
     * If this value is not positive, then the scheduler will choose the number of threads that
     * it will use.  The algorithm used to select the default thread count is left unspecified,
     * so it is recommended that applications provide an explicit value rather than accept the
     * default.
     * </p>
     *
     * @return the number of worker threads to use, or a non-positive value to accept the default value
     */
    int workerThreadCount();

    /**
     * If {@code true}, then the serialized parameter maps returned by {@code ClusteredJobDao} will
     * be allowed to contain instances of Quartz's {@code JobDataMap} class, which will be removed
     * transparently.
     * <p>
     * This is intended to make it easier to migrate from Quartz to Caesium by allowing the job data
     * to be cleaned lazily instead of requiring the Quartz classes to be removed from the data up front.
     * There may be a minor performance penalty to this support, as the deserialization code must
     * intercept the requests for the Quartz classes by comparing the names of each class as it is
     * resolved, but this is not expected to be significant.
     * </p>
     *
     * @return {@code true} to enable transparent migration of Quartz data returned by the
     * {@code ClusteredJobDao}; {@code false} to disable this feature.
     */
    boolean useQuartzJobDataMapMigration();

    /**
     * If {@code true}, then the scheduler will accept schedules that are more fine-grained than
     * once per minute.
     * <p>
     * The {@code atlassian-scheduler} library is permitted to "adjust" schedules that would run
     * more frequently than once a minute to prevent abuse.  Setting this to {@code true} will
     * disable this restriction, allowing intervals shorter than one minute and cron expressions
     * that specify values other than a single integer from 0 to 59 for the seconds field.
     * </p>
     *
     * @return {@code true} to allow all valid schedule requests, regardless of how frequently
     * they will run; {@code false} if schedules should be adjusted to prevent schedules
     * from running more frequently than once a minute.
     */
    boolean useFineGrainedSchedules();
}
