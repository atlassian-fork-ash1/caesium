package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.caesium.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.cron.CronExpressionValidator;
import com.atlassian.scheduler.cron.CronSyntaxException;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionValidator implements CronExpressionValidator {
    @Override
    public boolean isValid(String cronExpression) {
        return CronExpressionParser.isValid(cronExpression);
    }

    @Override
    public void validate(String cronExpression) throws CronSyntaxException {
        CronExpressionParser.parse(cronExpression);
    }
}
