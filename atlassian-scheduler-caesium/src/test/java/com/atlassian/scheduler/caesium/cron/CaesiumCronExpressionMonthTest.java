package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionMonthTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionMonthTest extends CronExpressionMonthTest {
    public CaesiumCronExpressionMonthTest() {
        super(new CaesiumCronFactory());
    }
}
