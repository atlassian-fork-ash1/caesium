package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.config.JobId;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.function.Consumer;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;


public class SchedulerQueueWorkerTest {
    @Rule
    public MockitoRule init = MockitoJUnit.rule();

    @Mock
    private SchedulerQueue queue;
    @Mock
    private Consumer<QueuedJob> executeJob;

    @InjectMocks
    private SchedulerQueueWorker worker;

    @Test
    public void testRun() throws InterruptedException {
        final long now = System.currentTimeMillis();
        final InterruptedException expected = new InterruptedException("Just testing");
        final QueuedJob job1 = new QueuedJob(JobId.of("job1"), now + 1000L);
        final QueuedJob job2 = new QueuedJob(JobId.of("job2"), now + 2000L);

        when(queue.take())
                .thenReturn(job1)
                .thenThrow(expected)
                .thenReturn(job2)
                .thenReturn(null);

        worker.run();

        verify(queue, times(4)).take();
        verify(executeJob).accept(job1);
        verify(executeJob).accept(job2);
        verifyNoMoreInteractions(queue, executeJob);
    }

    @Test
    public void testSurvivesNastyErrors() throws InterruptedException {
        final long now = System.currentTimeMillis();
        final QueuedJob job1 = new QueuedJob(JobId.of("job1"), now + 1000L);
        final QueuedJob job2 = new QueuedJob(JobId.of("job2"), now + 2000L);
        final QueuedJob job3 = new QueuedJob(JobId.of("job3"), now + 3000L);

        when(queue.take())
                .thenReturn(job1)
                .thenReturn(job2)
                .thenReturn(job3)
                .thenReturn(null);
        doThrow(new RuntimeException("Should be caught and logged")).when(executeJob).accept(job1);
        doThrow(new Error("Should be caught and logged")).when(executeJob).accept(job2);

        worker.run();

        verify(queue, times(4)).take();
        verify(executeJob).accept(job1);
        verify(executeJob).accept(job2);
        verify(executeJob).accept(job3);
        verifyNoMoreInteractions(queue, executeJob);
    }

}