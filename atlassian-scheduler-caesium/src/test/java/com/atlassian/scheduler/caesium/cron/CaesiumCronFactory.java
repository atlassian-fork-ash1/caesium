package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.caesium.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.caesium.cron.rule.CronRule;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.core.tests.CronFactory;
import com.atlassian.scheduler.cron.CronSyntaxException;
import org.joda.time.DateTimeZone;
import org.joda.time.ReadableDateTime;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * An adapter that maps Caesium's cron expression handling onto the core tests' specification.
 *
 * @since v0.0.1
 */
public class CaesiumCronFactory implements CronFactory {
    @Override
    public void parseAndDiscard(String cronExpression) {
        parseInternal(cronExpression);
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression) {
        return parse(cronExpression, DateTimeZone.getDefault());
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression, final DateTimeZone zone) {
        final CronRule rule = parseInternal(cronExpression);
        return new CronExpressionAdapter() {
            @Override
            public boolean isSatisfiedBy(Date date) {
                return rule.matches(new DateTimeTemplate(date, zone));
            }

            @Nullable
            @Override
            public Date nextRunTime(Date date) {
                final DateTimeTemplate dateTime = new DateTimeTemplate(date, zone);
                while (rule.next(dateTime)) {
                    final ReadableDateTime result = dateTime.toDateTime();
                    if (result != null) {
                        return new Date(result.getMillis());
                    }
                }
                return null;
            }

            @Override
            public String toString() {
                return cronExpression;
            }
        };
    }

    private static CronRule parseInternal(final String cronExpression) {
        try {
            return CronExpressionParser.parse(cronExpression);
        } catch (CronSyntaxException cse) {
            final AssertionError err = new AssertionError("Unable to parse cron expression '" + cronExpression + '\'');
            err.initCause(cse);
            throw err;
        }
    }
}
