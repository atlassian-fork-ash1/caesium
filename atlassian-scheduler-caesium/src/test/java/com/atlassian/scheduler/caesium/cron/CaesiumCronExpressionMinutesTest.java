package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionMinutesTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionMinutesTest extends CronExpressionMinutesTest {
    public CaesiumCronExpressionMinutesTest() {
        super(new CaesiumCronFactory());
    }
}
