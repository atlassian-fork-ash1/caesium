package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.caesium.impl.SchedulerQueue.SchedulerShutdownException;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.utt.concurrency.Barrier;
import com.atlassian.utt.concurrency.TestThread;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import static com.atlassian.utt.concurrency.TestThread.runTest;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SchedulerQueueImplTest {
    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of("JobRunnerKey");
    private static final JobId JOB_ID_1 = JobId.of("JobId1");
    private static final JobId JOB_ID_2 = JobId.of("JobId2");
    private static final JobId JOB_ID_3 = JobId.of("JobId3");
    private static final JobId JOB_ID_4 = JobId.of("JobId4");

    @Rule
    public MockitoRule init = MockitoJUnit.rule();
    @Rule
    public ExpectedException expect = ExpectedException.none();

    @Mock
    private Lock lock;
    @Mock
    private Condition awaken;

    private ClusteredJobDao clusteredJobDao = new MemoryClusteredJobDao();
    private SchedulerQueueImpl queue;
    volatile long now;

    @Before
    public void setUp() {
        when(lock.newCondition()).thenReturn(awaken);

        queue = new SchedulerQueueImpl(clusteredJobDao, lock) {
            @Override
            long now() {
                return now;
            }
        };
    }


    @Test
    public void testAddAndGetPendingJobs() throws Exception {
        now = 10000L;

        final QueuedJob job1 = new QueuedJob(JOB_ID_1, now);
        final QueuedJob job2 = new QueuedJob(JOB_ID_2, now + 1000L);
        final QueuedJob job3 = new QueuedJob(JOB_ID_3, now - 1000L);

        queue.add(job1);
        verify(awaken).signalAll();

        queue.add(job2);
        verify(awaken).signalAll();  // still only once

        queue.add(job3);
        verify(awaken, times(2)).signalAll();

        assertThat(queue.getPendingJobs(), contains(job3, job1, job2));
    }

    @Test
    public void testRefreshClusteredJobs() throws Exception {
        now = 10000L;
        queue.add(job(JOB_ID_1, now - 42L));
        queue.add(job(JOB_ID_4, now + 42L));

        final QueuedJob job1 = new QueuedJob(JOB_ID_1, now);
        final QueuedJob job2 = new QueuedJob(JOB_ID_2, now + 1000L);
        final QueuedJob job3 = new QueuedJob(JOB_ID_3, now - 1000L);
        final QueuedJob job4 = new QueuedJob(JOB_ID_4, now + 42L);

        clusteredJobDao.create(clusteredJob(JOB_ID_1, JOB_RUNNER_KEY, Schedule.runOnce(null), new Date(now), 1L));
        clusteredJobDao.create(clusteredJob(JOB_ID_2, JOB_RUNNER_KEY, Schedule.runOnce(null), new Date(now + 1000L), 1L));
        clusteredJobDao.create(clusteredJob(JOB_ID_3, JOB_RUNNER_KEY, Schedule.runOnce(null), new Date(now - 1000L), 1L));

        final Map<JobId, Date> expected = ImmutableMap.<JobId, Date>builder()
                .put(JOB_ID_1, new Date(now))
                .put(JOB_ID_2, new Date(now + 1000L))
                .put(JOB_ID_3, new Date(now - 1000L))
                .build();

        reset(awaken);  // Don't care about any signals during the setup

        assertThat(queue.refreshClusteredJobs(), equalTo(expected));
        assertThat(queue.getPendingJobs(), contains(job3, job1, job4, job2));
        verify(awaken).signalAll();

        assertThat(queue.refreshClusteredJobs(), equalTo(expected));
        verify(awaken).signalAll();  // Still only once

        clusteredJobDao.create(clusteredJob(JOB_ID_4, JOB_RUNNER_KEY, Schedule.runOnce(null), new Date(now - 3000L), 1L));
        queue.refreshClusteredJobs();
        verify(awaken, times(2)).signalAll();

        assertThat(queue.getPendingJobs(), contains(job(JOB_ID_4, now - 3000L), job3, job1, job2));
    }

    @Test
    public void testRemove() throws Exception {
        now = 10000L;
        final QueuedJob job1 = job(JOB_ID_1, now);
        final QueuedJob job2 = job(JOB_ID_2, now + 1000L);
        final QueuedJob job3 = job(JOB_ID_3, now);

        queue.add(job1);
        verify(awaken).signalAll();

        queue.add(job2);
        assertThat(queue.getPendingJobs(), contains(job1, job2));
        verify(awaken).signalAll();  // Still only once

        queue.remove(JOB_ID_3);
        assertThat(queue.getPendingJobs(), contains(job1, job2));
        verify(awaken).signalAll();  // Still only once

        queue.remove(JOB_ID_3);
        assertThat(queue.getPendingJobs(), contains(job1, job2));
        verify(awaken).signalAll();  // Still only once

        queue.remove(JOB_ID_1);
        assertThat(queue.getPendingJobs(), contains(job2));
        verify(awaken).signalAll();  // Still only once

        queue.remove(JOB_ID_2);
        assertThat(queue.getPendingJobs(), Matchers.<QueuedJob>hasSize(0));
        verify(awaken).signalAll();  // Still only once

        queue.add(job3);
        assertThat(queue.getPendingJobs(), contains(job3));
        verify(awaken, times(2)).signalAll();
    }

    @Test
    public void testClose() throws Exception {
        now = 10000L;
        final QueuedJob job1 = new QueuedJob(JOB_ID_1, now + 5000L);
        queue.add(job1);
        verify(awaken).signalAll();
        assertThat(queue.isClosed(), is(false));

        final Barrier takeBarrier = new Barrier();
        final Barrier closedBarrier = new Barrier();
        final Thread thd = new Thread("Test thread: calls queue.close() for us") {
            @Override
            public void run() {
                try {
                    takeBarrier.await();
                    queue.close();
                    closedBarrier.signal();
                } catch (Exception ex) {
                    throw new AssertionError(ex);
                }
            }
        };
        thd.start();

        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                takeBarrier.signal();
                closedBarrier.await();
                return null;
            }
        }).when(awaken).await(5000L, TimeUnit.MILLISECONDS);

        assertThat(queue.getPendingJobs(), contains(job1));
        assertThat(queue.take(), nullValue());
        assertThat(queue.isClosed(), is(true));
        verify(awaken, times(2)).signalAll();
        assertThat(queue.getPendingJobs(), hasSize(0));

        expect.expect(SchedulerShutdownException.class);
        queue.add(new QueuedJob(JOB_ID_2, now - 1000L));
    }

    @Test
    public void testPauseAndResume() throws Exception {
        now = 10000L;
        queue.resume();
        queue.resume();
        verify(awaken, never()).signalAll();

        final QueuedJob job1 = new QueuedJob(JOB_ID_1, now - 1000L);
        queue.add(job1);
        verify(awaken).signalAll();

        final QueuedJob job2 = new QueuedJob(JOB_ID_2, now - 5000L);
        queue.pause();
        queue.add(job2);
        verify(awaken).signalAll();  // still only once

        queue.pause();  // Should be a no-op

        final Barrier takeBarrier = new Barrier();
        final Barrier resumeBarrier = new Barrier();
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                takeBarrier.signal();
                resumeBarrier.await();
                return null;
            }
        }).when(awaken).await();

        final TestThread thd1 = new TestThread("thd1: calls queue.resume()") {
            @Override
            public void go() throws SchedulerShutdownException {
                takeBarrier.await();
                queue.resume();
                resumeBarrier.signal();
            }
        };

        final TestThread thd2 = new TestThread("thd2: waits for queue.resume()") {
            @Override
            protected void go() throws Exception {
                assertThat(queue.take(), equalTo(job2));
            }
        };

        runTest(thd1, thd2);
        assertThat(queue.take(), equalTo(job1));
    }


    private static ImmutableClusteredJob clusteredJob(JobId jobId, JobRunnerKey jobRunnerKey, Schedule schedule,
                                                      @Nullable Date nextRunTime, long version) {
        return ImmutableClusteredJob.builder()
                .jobId(jobId)
                .jobRunnerKey(jobRunnerKey)
                .schedule(schedule)
                .nextRunTime(nextRunTime)
                .version(version)
                .build();
    }

    private static QueuedJob job(final JobId jobId, final long deadline) {
        return new QueuedJob(jobId, deadline);
    }
}