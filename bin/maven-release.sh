#!/bin/sh

#
# This file is managed by https://bitbucket.org/atlassian/armata-pipelines-primer/src/mvn-pac/.
#

set -ex

# Prepare and perform the maven release
mvn --batch-mode --errors release:prepare -DscmCommentPrefix="[skip ci] " -DautoVersionSubmodules=true

# Release the artifact to artifactory
mvn --batch-mode --errors release:perform -DlocalCheckout=true
