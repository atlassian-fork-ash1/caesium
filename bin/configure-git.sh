#!/bin/sh

#
# This file is managed by https://bitbucket.org/atlassian/armata-pipelines-primer/src/mvn-pac/.
#

git config --global user.email "armata-build-bot@atlassian.com"
git config --global user.name "Armata Build Bot (Bitbucket Pipelines)"
