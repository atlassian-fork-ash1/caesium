## Overview ##

The `atlassian-scheduler` library is an API for creating scheduled tasks in
Atlassian applications.  The `atlassian-scheduler-caesium` implementation of
this library is built from scratch to implement the base library in terms of
a simple cron expression parsing and evaluation system, scheduling queue, and
database object abstraction.

## About Caesium ##

Caesium is like Quartz, in that it can be used to keep track of scheduled things.
However, it is much simpler and hopefully does not have many bugs.

The name derives from the fact that since 1967 the definition of 1 second has
been "The duration of 9,192,631,770 periods of the radiation corresponding to
the transition between the two hyperfine levels of the ground state of the
caesium-133 atom."

Note: I would have preferred to spell this "cesium," but IUPAC says otherwise.

## Builds ##

This project requires Maven 3.

* Issue tracking: https://bitbucket.org/atlassian/caesium/issues
* Builds (Internal): https://ecosystem-bamboo.internal.atlassian.com/browse/SCHEDCAES

## Copyright ##

Copyright @ 2015 Atlassian Pty Ltd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

